#!/bin/bash
#SBATCH --account=mh0066
#SBATCH --partition=shared
#SBATCH --time=07-00:00:00
#SBATCH --mem=32GB
#SBATCH --ntasks-per-node=4
#SBATCH --job-name=archive
#SBATCH --dependency=singleton
#SBATCH --output=LOG.%x.%j.o

set -o errexit -o nounset

module purge
module load slk

set -o xtrace

num_procs=4
num_files_per_job=16
arch_path="/arch/bd1154/highresmonsoon/$1"

slk_helpers exists "${arch_path}" || slk_helpers mkdir -R "${arch_path}"
find $1 -iname *.grb2 | xargs -P${num_procs} -n${num_files_per_job} bash -c 'slk archive -vv $* $0' "${arch_path}"
