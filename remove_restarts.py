#!/usr/bin/env python3
import argparse
import pathlib
import os


def rmtree(root):
    """Remove a directory and all its content recursively."""
    for p in root.iterdir():
        if p.is_dir():
            rmtree(p)
        else:
            p.unlink()
    root.rmdir()


def find_multi_file_restarts(root, regex="*_restart_atm_*.mfr"):
    """Find all multi-file restarts in a given directory."""
    experiment_dir = pathlib.Path(root)
    return [p for p in sorted(experiment_dir.glob(regex)) if p.is_dir()]


def remove_multifile_restarts(root, keep_restarts=1):
    """Remove all but `keep_restarts` multi-file restarts in a given directory."""
    keep_restarts = None if keep_restarts < 1 else -keep_restarts
    restarts = find_multi_file_restarts(root)
    for restart in restarts[:keep_restarts]:
        print(f"rm {restart.as_posix()}")
        rmtree(restart)


def main():
    parser = argparse.ArgumentParser(
        description="Remove multi-file restarts from a given experiment directory."
    )
    parser.add_argument("exp_dir", type=str, help="experiment directory")
    parser.add_argument(
        "-k",
        "--keep",
        type=int,
        default=1,
        help="number of multi-file restarts to keep",
    )

    args = parser.parse_args()

    remove_multifile_restarts(root=args.exp_dir, keep_restarts=args.keep)


if __name__ == "__main__":
    main()
