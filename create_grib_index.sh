#!/bin/bash
#SBATCH --job-name=grib-index
#SBATCH --partition=compute
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=128
#SBATCH --exclusive
#SBATCH --time=01:00:00
#SBATCH --account=mh0066
#SBATCH --output=LOG.%x.%j.out

set -o xtrace -o nounset

gribscan-index -n ${SLURM_NTASKS_PER_NODE} ${1}/*.grb2
gribscan-build ${1}/*.index -o ${1}/
