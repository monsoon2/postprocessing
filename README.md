# Monsoon 2.0 - Data transfer & postprocessing

This repository contains scripts to transfer data between JSC and DKRZ as well
as some postprocessing of ICON output.
