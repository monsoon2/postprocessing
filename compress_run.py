#!/usr/bin/env python3
#
#SBATCH --job-name=grib_set
#SBATCH --partition=booster
#SBATCH --gres=gpu:4
#SBATCH --time=24:00:00
#SBATCH --account=highresmonsoon
#SBATCH --output=LOG.compress.%j
#SBATCH --error=LOG.compress.%j
#SBATCH --open-mode=append

import glob
import os
import sys
from subprocess import run
from multiprocessing import Pool


def compress_file(grbfilein):
    """Enable AEC compression for GRIB2 files and delete original file."""
    grbfileout = grbfilein.replace(".grb", ".grb2")

    args = ["grib_set", "-s", "packingType=grid_ccsds", grbfilein ,grbfileout]

    print("+ ", " ".join(args), flush=True)
    run(args, check=True)

    os.remove(grbfilein)


runid = sys.argv[1]

with Pool(24) as pool:
    pool.map(compress_file, glob.iglob(f"{runid}/*.grb"))
