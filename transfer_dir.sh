#!/bin/bash

set -o errexit -o nounset -o xtrace

# Run description
run_id=$1

# UFTP settings
UFTP_USER=kluft1
UFTP_AUTH_URL=https://uftp.fz-juelich.de:9112/UFTP_Auth/rest/auth/JUDAC:
UFTP_KEY=$HOME/.ssh/id_ed25519_judac

mkdir -p $run_id  # Create output directory

# Multi-threaded copy of all data for the given month
uftp cp \
  --verbose \
  --user $UFTP_USER \
  --identity $UFTP_KEY \
  --recurse \
  --threads 4 \
  --streams 4 \
  --split-threshold 32G \
  "$UFTP_AUTH_URL/p/scratch/highresmonsoon/experiments/$run_id/*" \
  $run_id
