#!/usr/bin/env python3
#SBATCH --job-name=grib2zarr
#SBATCH --partition=shared
#SBATCH --ntasks=1
#SBATCH --time=00:30:00
#SBATCH --account=bd1154
#SBATCH --output=logs/LOG.%x.%A_%a.o
#SBATCH --parsable
import argparse
import itertools
import json
import multiprocessing
import operator
import os
import pathlib
import random
from functools import partial

import dask.array
import fsspec
import numcodecs
import numpy as np
import xarray as xr
import yaml
import zarr


bitround_yaml = "https://gitlab.gwdg.de/ican/bitround_params/-/raw/main/keepbits/default.yaml"
with fsspec.open(bitround_yaml) as kbf:
    keepbits_per_variable = yaml.load(kbf, Loader=yaml.SafeLoader)


def get_chunks(dimensions):
    if any([dim.endswith("level") for dim in dimensions]):
        chunks = {
            "time": 12,
            "cell": 4 ** 9,
            "fulllevel": 4,
            "halflevel": 4,
        }
    else:
        chunks = {
            "time": 48,
            "cell": 4 ** 9,
        }

    return tuple((chunks[d] for d in dimensions))


def get_encoding(dataset):
    compressor = numcodecs.Blosc("zstd")

    return {
        var: {
            "compressor": compressor,
            "dtype": "float32",
            "chunks": get_chunks(dataset[var].dims),
        }
        for var in dataset.variables
        if var not in dataset.dims
    }


def create_store(source, target):
    print(f"Create {target}", flush=True)
    ds = xr.open_zarr(source, decode_cf=False, consolidated=False)

    # Create dummy dataset with monolithic input chunk.
    for name, var in ds.items():
        # HACHK: Overwrite bitrounding threshold until a robust one is found.
        var.attrs["keepbits"] = 14  # keepbits_per_variable.get(name, 10)
        ds = ds.assign(
            **{
                name: (
                    var.dims,
                    dask.array.empty(
                        shape=var.shape, chunks=var.shape, dtype=var.dtype
                    ),
                    var.attrs,
                )
            }
        )
    ds.attrs["history"] += (
        r"Applied numcodecs bit rounding."
        " See variables for `keepbits` information.\n"
    )

    ds.to_zarr(
        target,
        mode="w-",
        encoding=get_encoding(ds),
        compute=False,
    )

    for zarray in pathlib.Path(target).glob("*/.zarray"):
        with open(zarray, "r") as fp:
            array_spec = json.load(fp)

        array_spec["dimension_separator"] = "/"

        with open(zarray, "w") as fp:
            json.dump(array_spec, fp)

    zarr.consolidate_metadata(target)


def write_schedule(source, target):
    print(f"Create conversion schedule from {source} -> {target}", flush=True)
    source_group = zarr.open_group(source)
    target_group = zarr.open_group(target)

    schedule = {
        "source": source,
        "target": target,
        "tasks": [],
    }
    for var in source_group:
        vs = source_group[var]
        vt = target_group[var]

        blocksize = tuple(np.maximum(vs.chunks, vt.chunks).tolist())

        for s in itertools.product(
            *[range(0, sp, bs) for sp, bs in zip(vs.shape, blocksize)]
        ):
            schedule["tasks"].append([var, [(a, a + b) for a, b in zip(s, blocksize)]])

    with open(pathlib.Path(target) / "schedule.json", "w") as fp:
        random.shuffle(schedule["tasks"])
        json.dump(schedule, fp)


def write_region(task_tuple, source, target):
    task_id, task = task_tuple  # Unpack task id and task
    var, region = task

    # Check if task is already done.
    ready_dir = pathlib.Path(f"{target}/.ready/{var}")
    ready_dir.mkdir(exist_ok=True, parents=True)
    ready_file = ready_dir / f"{task_id}"
    if ready_file.exists():
        print(f"Task {task_id} already executed. Skip.", flush=True)
        return

    # Extract data region
    index = tuple(slice(*limit) for limit in region)
    source_group = zarr.open_group(source)
    data = source_group[var][index]

    # Set invalid values to NaN
    is_nan = data == source_group[var].fill_value
    if np.any(is_nan):
        data[is_nan] = np.nan

    # Skip NaN-only chunks
    if np.sum(is_nan) == data.size:
        ready_file.touch()
        print("Only NaNs. Skip.", flush=True)
        return

    target_group = zarr.open_group(target)
    # Apply bit-rounding for variables
    print(f"{var}: {index}", flush=True)
    try:
        rounding = numcodecs.BitRound(keepbits=target_group[var].attrs["keepbits"])
        data = rounding.decode(rounding.encode(data.copy()))
    except KeyError:
        # This only happens for coordinates (which are already written anyhow).
        pass

    # Copy data chunks.
    target_group[var][index] = data

    ready_file.touch()


def main():
    parser = argparse.ArgumentParser(description="Convert an Xarray dataset to Zarr.")
    subparsers = parser.add_subparsers(help="sub-command help", dest="command")

    parser_a = subparsers.add_parser(
        "prepare", help="prepare a schedule to convert a dataset"
    )
    parser_a.add_argument("source", metavar="source", type=str)
    parser_a.add_argument("target", metavar="target", type=str)

    parser_b = subparsers.add_parser("run", help="perform a schedule conversion")
    parser_b.add_argument("schedule", metavar="source", type=str)
    parser_b.add_argument("nprocs", metavar="nprocs", type=int, default=1)

    args = parser.parse_args()

    if args.command == "prepare":
        create_store(args.source, args.target)
        write_schedule(args.source, args.target)

    if args.command == "run":
        with open(args.schedule, "r") as fp:
            schedule = json.load(fp)

        # Get information about job array
        job_id = int(os.environ.get("SLURM_ARRAY_TASK_ID", "0"))
        njobs = int(os.environ.get("SLURM_ARRAY_TASK_COUNT", "1"))

        ntasks = len(schedule["tasks"])
        job_chunk = int(np.ceil(ntasks / njobs))

        # Get information from pre-created conversion schedule
        source = schedule["source"]
        target = schedule["target"]
        tasks = schedule["tasks"]

        task_ids = range(
            job_id * job_chunk,
            min((job_id + 1) * job_chunk, ntasks)
        )
        get_tasks = operator.itemgetter(*list(task_ids))

        with multiprocessing.Pool(args.nprocs) as pool:
            pool.map(
                partial(write_region, source=source, target=target),
                zip(task_ids, get_tasks(tasks)),
            )


if __name__ == "__main__":
    import socket
    print(socket.gethostname())

    main()
