#!/usr/bin/env python3
import pathlib
from collections import defaultdict

import intake
import yaml
import zarr


def get_info(runid):
    with open(
        f"/work/bd1154/highresmonsoon/experiments/{runid}/exp.{runid}.yaml", "r"
    ) as fp:
        return yaml.safe_load(fp)


expdir = pathlib.Path("/work/bd1154/highresmonsoon/experiments_zarr/")

# Collect available runs and their output groups
experiments = defaultdict(list)
for zarr_store in sorted(expdir.glob("*.zarr")):
    exp_id, out_type = zarr_store.name.split(".")[0].split("_")

    experiments[exp_id].append(out_type)

# Create an intake catalog for each run listing its output groups
for exp_id, output_types in experiments.items():
    meta = dict(get_info(exp_id))
    cat = {
        "sources": {},
        "metadata": meta,
    }

    for out_type in output_types:
        zarr_store = (expdir / f"{exp_id}_{out_type}.zarr").as_posix()
        zg = zarr.open(zarr_store, mode="r")

        cat["sources"][out_type] = {
            "args": {
                "urlpath": zarr_store,
                "chunks": {"time": 48},
                "consolidated": True,
            },
            "driver": "zarr",
            "metadata": {**meta, **zg.attrs},
        }

    with open(expdir / (exp_id + "_catalog.yaml"), "w") as fp:
        fp.write(yaml.safe_dump(cat))


cat = {
    "sources": {
        "grids": {
            "descriptions": "Catalog of ICON grids",
            "driver": "yaml_file_cat",
            "args": {
                "path": "https://gitlab.gwdg.de/ican/catalogs/-/raw/main/grids/icon_grids_by_uuid.yaml"
            },
        }
    }
}

# Create a main catalog listing all run catalogs
for catalog in expdir.glob("*_catalog.yaml"):
    exp_id = catalog.name.split("_")[0]

    cat["sources"][exp_id] = {
        "args": {"path": catalog.as_posix()},
        "description": f"Monsoon 2.0 run {exp_id}. Check `metadata` for more information.",
        "driver": "yaml_file_cat",
        "metadata": intake.open_catalog(catalog).metadata,
    }

with open(expdir / "../monsoon2_zarr.yaml", "w") as fp:
    fp.write(yaml.safe_dump(cat))
