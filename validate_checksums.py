#!/usr/bin/env python3
#SBATCH --job-name=sha256_validate
#SBATCH --partition=shared
#SBATCH --time=24:00:00
#SBATCH --account=bd1154
#SBATCH --output=LOG.%x.%j
#SBATCH --error=LOG.%x.%j
import argparse
import hashlib
import multiprocessing
import os
import socket
import sys
import time


def sha256sum(filename, chunk_size=16_384):
    """Calculate the SHA256 sum for a given file."""
    sha256_hash = hashlib.sha256()
    with open(filename, "rb") as f:
        while True:
            byte_block = f.read(chunk_size)

            if byte_block:
                sha256_hash.update(byte_block)
            else:
                break

    return sha256_hash.hexdigest()


def verify_sha256_line(line):
    """Verify SHA256 sum of a single entry in a SHA256 file."""
    hexdigest, filename = line.split()
    filename = filename.lstrip("*")

    return filename, sha256sum(filename) == hexdigest


def verify_sha256_file(sha_file, nprocesses=8):
    """Verify all entries in an SHA256 sum file."""
    with open(sha_file, "r") as f:
        sha_sums = f.readlines()

    os.chdir(os.path.dirname(sha_file))
    with multiprocessing.Pool(nprocesses) as pool:
        rets = dict(pool.map(verify_sha256_line, sha_sums))

    with open("sha256.log", "w") as f:
        f.write(time.asctime() + "\n")
        f.write(socket.gethostname() + "\n")
        for filename, code in rets.items():
            if code:
                f.write(f"{filename}: OK\n")
            else:
                f.write(f"{filename}: Failed\n")

    assert all(rets.values()), "Found invalid checksum!"


def _main():
    parser = argparse.ArgumentParser(description="Validate SHA256 sums in a file.")
    parser.add_argument(
        "-f",
        "--file",
        dest="sha256_file",
        type=str,
        help="File containing a filename-SHA256 pair per line",
    )
    parser.add_argument(
        "-n",
        dest="nprocesses",
        type=int,
        default=8,
        help="number of parallel processes",
    )

    args = parser.parse_args()

    verify_sha256_file(args.sha256_file, args.nprocesses)

if __name__ == "__main__":
    _main()
