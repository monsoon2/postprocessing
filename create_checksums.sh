#!/bin/bash
#
#SBATCH --job-name=checksums
#SBATCH --partition=booster
#SBATCH --gres=gpu:4
#SBATCH --time=04:00:00
#SBATCH --account=highresmonsoon
#SBATCH --output=LOG.checksums.%j
#SBATCH --error=LOG.checksums.%j

set -o errexit -o nounset -o xtrace

rundir=$1
cd $rundir

# Create SHA256 hash for all data files in experiment directory.
sha256sum -b *.grb2 *.nc > sha256.txt
